print("wellcome")
print("Ucl Erhvervsakademi IT Group")

name = input("Enter your name:")
print("Hello " + name + "!")

# Prompt for input variables
heightft = input("Enter your height (feet): ")
heightin = input("Enter your height (inches): ")
weight = input("Enter your weight (pounds): ")
# Ensure that entries are not blanks
if heightft == "" or heightin =="":
    print ("Please enter your height.")
    exit()
if weight == "":
    print ("Please enter your weight.")
    exit()
# Format entries as float data type
heightft = float(heightft)
heightin = float(heightin)
weight = float(weight)
# Handle abnormal entries for heights and weights
if heightft <0 or heightin <0:
    print ("You cannot possibly have a negative height!")
    exit()
if heightft < 3:
    print ("Standing " + str(heightft) + " feet tall you must be a child; this is an adult BMI calcuator!")
    exit()
if heightft > 10:
    print ("Are you really " + str(heightft) + " feet tall ?!")
    exit()
if heightin > 12:
    print ("Your height in inches shouldn't exceed 12!")
    exit()
if weight < 0:
    print ("Your weight can only be negative in Theoretical Physics..., NOT in real life.")
if weight  < 30:
    print ("Weighing " + str(weight) + " lbs, you must be a child; this is an adult BMI calcuator!")
    exit()
if weight > 1500:
    print ("Do you really weigh " + str(weight) + " pounds?!")
    exit()
print("Good day")
